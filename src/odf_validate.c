#include <dlfcn.h>
#include "public/com.litebmc.Connector.h"

static GHashTable *so_hash = NULL;
static GHashTable *result_hash = NULL;
typedef struct {
    gint result;
    gchar *sha256;
} validate_result;
G_LOCK_DEFINE_STATIC(lock);

static const gchar *_find_interface_public_so(const gchar *interface)
{
    gchar *so_path = g_hash_table_lookup(so_hash, interface);
    if (so_path) {
        return so_path;
    }
    const gchar *ld_path = g_getenv("LD_LIBRARY_PATH");
    if (!ld_path) {
        ld_path = "/usr/lib:/usr/lib64:/lib:/lib64:/opt/litebmc/lib";
    }
    cleanup_strv gchar **ld_dirs = g_strsplit(ld_path, ":", 0);
    GString *tmp = g_string_sized_new(128);
    g_string_printf(tmp, "lib%s-public.so", interface);
    tmp = g_string_ascii_down(tmp);
    cleanup_gfree gchar *so_name = g_string_free(tmp, FALSE);
    for (gint i = 0; ld_dirs && ld_dirs[i]; i++) {
        log_mass("Start search %s", ld_dirs[i]);
        DIR *dir = opendir(ld_dirs[i]);
        if (!dir) {
            log_debug("library directory not exist, msg: %s", strerror(errno));
            continue;;
        }
        while (TRUE) {
            struct dirent *ent = readdir(dir);
            if (ent == NULL) {
                break;
            }
            if (ent->d_type != DT_REG) {
                continue;
            }
            // 普通文件且so名称前缀匹配时找到正确的动态库
            if (strncmp(ent->d_name, so_name, strlen(so_name)) == 0) {
                gchar *so_path = lb_printf("%s/%s", ld_dirs[i], ent->d_name);
                closedir(dir);
                g_hash_table_insert(so_hash, g_strdup(interface), so_path);
                return so_path;
            }
        }
        closedir(dir);
    }
    return NULL;
}

static const gchar *_get_interface(yaml_document_t *doc, yaml_node_t *node)
{
    yaml_node_t *key;
    yaml_node_t *val;
    for (yaml_node_pair_t *pair = node->data.mapping.pairs.start; pair < node->data.mapping.pairs.top; pair++) {
        key = yaml_document_get_node(doc, pair->key);
        if (g_strcmp0((const gchar *)key->data.scalar.value, "interface") == 0) {
            val = yaml_document_get_node(doc, pair->value);
            if (val && val->type == YAML_SCALAR_NODE) {
                log_debug("get interface: %s", val->data.scalar.value);
                return (const gchar *)val->data.scalar.value;
            }
            log_warn("unexcept properties node get, key type: %d, need: 1(YAML_SCALE_SCALAR)", key->type);
            return NULL;
        }
    }
    return NULL;
}

static yaml_node_t *_get_properties(yaml_document_t *doc, yaml_node_t *node)
{
    yaml_node_t *key;
    for (yaml_node_pair_t *pair = node->data.mapping.pairs.start; pair < node->data.mapping.pairs.top; pair++) {
        key = yaml_document_get_node(doc, pair->key);
        if (g_strcmp0((const gchar *)key->data.scalar.value, "properties") == 0) {
            return yaml_document_get_node(doc, pair->value);
        }
    }
    return NULL;
}

static gboolean _validate_odf_object_by_so(yaml_document_t *doc, yaml_node_t *properties, const gchar *interface, const gchar *object_path, GSList **error_list)
{
    gboolean valid = FALSE;
    const gchar *so_path = _find_interface_public_so(interface);
    if (!so_path) {
        log_warn("unkown interface %s, skip validate object %s", interface, object_path);
        return FALSE;
    }
    void *dl = dlopen(so_path, RTLD_NOW);
    if (!dl) {
        log_warn("open so (%s) failed, validate object %s failed, msg: %s", so_path, object_path, dlerror());
        return FALSE;
    }
    GString *validate_func = g_string_new(interface);
    g_string_replace(validate_func, ".", "_", 0);
    g_string_append(validate_func, "_validate_odf");
    lbo_validate_odf validate_odf = (lbo_validate_odf)dlsym(dl, validate_func->str);
    if (validate_odf) {
        valid = validate_odf(doc, properties, object_path, error_list);
    } else {
        log_warn("symbol %s is missing, validate object %s failed, so path: %s", validate_func->str, object_path, so_path);
    }
    g_string_free(validate_func, TRUE);
    dlclose(dl);
    return valid;
}

static gboolean _validate_odf_object_by_intf(yaml_document_t *doc, yaml_node_t *properties, const gchar *interface, const gchar *object_path, GSList **error_list)
{
    gboolean valid = FALSE;
    GSList *intfs = lb_interfaces_origin();
    for (GSList *item = intfs; item; item = item->next) {
        LBInterface *intf = (LBInterface *)item->data;
        // 接口名不匹配或不是服务端接口的
        if (g_strcmp0(intf->name, interface) || intf->is_remote) {
            continue;
        }
        // 只在验证接口存在时验证，否则返回成功
        if (intf->validate_odf) {
            valid = intf->validate_odf(doc, properties, object_path, error_list);
            break;
        }
    }
    g_slist_free(intfs);
    return valid;
}

static gint _validate_odf_object(yaml_document_t *doc, yaml_node_t *node, const gchar *object_path)
{
    const gchar *interface = _get_interface(doc, node);
    if (interface == NULL) {
        log_warn("interface is missing, skip validate object %s", interface, object_path);
        return -1;
    }
    if (!g_regex_match(lb_interface_regex(), interface, 0, NULL)) {
        log_warn("interface(%s) not a valid interface, skip validate object %s", interface, object_path);
        return -1;
    }
    yaml_node_t *properties = _get_properties(doc, node);
    // 属性不存在时认为验证通过
    if (!properties) {
        return 0;
    }
    if (properties->type != YAML_MAPPING_NODE) {
        log_warn("unexcept properties type: %d, need: 3(YAML_SCALE_MAPPING), validate object %s failed", properties->type, object_path);
        return -1;
    }
    GSList *error_list = NULL;
    gboolean ret = _validate_odf_object_by_intf(doc, properties, interface, object_path, &error_list);
    if (ret == TRUE) {
        return 0;
    } else if (error_list) {
        // error_list不为空，表示找到了接口并且验证失败
        for (GSList *item = error_list; item; item = item->next) {
            GError *error = item->data;
            log_warn("%s", error->message);
        }
        g_slist_free_full(error_list, (GDestroyNotify)g_error_free);
        error_list = NULL;
        return -1;
    }

    ret = _validate_odf_object_by_so(doc, properties, interface, object_path, &error_list);
    if (ret != TRUE) {
        // 验证失败时打印日志
        for (GSList *item = error_list; item; item = item->next) {
            GError *error = item->data;
            log_warn("%s", error->message);
        }
        g_slist_free_full(error_list, (GDestroyNotify)g_error_free);
        error_list = NULL;
        return -1;
    }
    return 0;
}

static gint _validate_odf_objects(yaml_document_t *doc, yaml_node_t *node, const gchar *odf_file)
{
    gint result = 0;
    yaml_node_t *key;
    yaml_node_t *val;
    for (yaml_node_pair_t *pair = node->data.mapping.pairs.start; pair < node->data.mapping.pairs.top; pair++) {
        key = yaml_document_get_node(doc, pair->key);
        val = yaml_document_get_node(doc, pair->value);
        if (key->type != YAML_SCALAR_NODE || val->type != YAML_MAPPING_NODE) {
            log_warn("unexcept node get, key type: %d, val type: %d", key->type, val->type);
            result = -1;
            continue;
        }
        log_mass("object name: %s", key->data.scalar.value);
        const gchar *object_path = (const gchar *)key->data.scalar.value;
        if (!g_regex_match(lb_obj_regex(), object_path, 0, NULL)) {
            log_warn("object %s not a valid object, skip validate", object_path);
            result = -1;
            continue;
        }
        if (_validate_odf_object(doc, val, object_path) != 0) {
            log_error("validate object (%s) failed, odf file: %s", object_path, odf_file);
            result = -1;
            continue;
        }
    }
    return result;
}

static gint _validate_odf(yaml_document_t *doc, yaml_node_t *node, const gchar *odf_file)
{
    yaml_node_t *key;
    yaml_node_t *val;
    yaml_node_t *objects = NULL;
    const gchar *service = NULL;
    const gchar *bus_type = NULL;
    for (yaml_node_pair_t *pair = node->data.mapping.pairs.start; pair < node->data.mapping.pairs.top; pair++) {
        key = yaml_document_get_node(doc, pair->key);
        val = yaml_document_get_node(doc, pair->value);
        if (key->type == YAML_SCALAR_NODE) {
            if (strcmp((const gchar *)key->data.scalar.value, "service") == 0) {
                service = (const gchar *)val->data.scalar.value;
            } else if (strcmp((const gchar *)key->data.scalar.value, "bus_type") == 0) {
                bus_type = (const gchar *)val->data.scalar.value;
            } else if (strcmp((const gchar *)key->data.scalar.value, "objects") == 0) {
                objects = val;
            }
        }
    }
    if (!service) {
        log_warn("service name is missing, validate %s failed", odf_file);
        return -1;
    }
    if (!bus_type) {
        log_warn("bus_type name is missing, validate %s failed", odf_file);
        return -1;
    }
    if (!g_regex_match(lb_service_regex(), service, 0, NULL)) {
        log_warn("service name (%s) not match with pattern " ODF_SERVICE_REGEX_STR ", validate %s failed",
            service, odf_file);
        return -1;
    }
    if (g_strcmp0(bus_type, "session") && g_strcmp0(bus_type, "system")) {
        log_warn("bus_type (%s) neither session nor system, validate %s failed", odf_file);
        return -1;
    }
    if (!objects) {
        log_warn("objects is missing, validate %s failed", odf_file);
        return -1;
    }
    if (objects->type != YAML_MAPPING_NODE) {
        log_warn("the objects is not object type, yaml type: %d", objects->type);
        return -1;
    }
    return _validate_odf_objects(doc, objects, odf_file);
}

static gint _validate_odf_file(const gchar *odf_file, const gchar *content)
{
    log_info("Start validated odf file(%s)", odf_file);
    yaml_parser_t parser;
    yaml_document_t document;
    yaml_node_t *node = NULL;
    gint ret = 0;

    yaml_parser_initialize(&parser);
    yaml_parser_set_input_string(&parser, (const unsigned char *)content, strlen(content));

    do {
        if (!yaml_parser_load(&parser, &document)) {
            log_error("Parse file %s failed, maybe not yaml format", odf_file);
            ret = -1;
            break;
        }

        node = yaml_document_get_root_node(&document);
        if (node && node->type == YAML_MAPPING_NODE) {
            if (_validate_odf(&document, node, odf_file) != 0) {
                ret = -1;
            }
        }

        yaml_document_delete(&document);

    } while (node);

    yaml_parser_delete(&parser);
    return ret;
}

/**
 * @brief  计算文件的sha256值
 * @note
 * @param  *odf_file: 待处理的文件路径
 * @param  **content: 返回值，计算成功时用于返回文件内容
 * @retval 文件的sha256值
 */
static gchar *_get_file_sha256(const gchar *odf_file, gchar **content)
{
    cleanup_fclose FILE *fp = fopen(odf_file, "rb");
    if (!fp) {
        log_notice("Open %s failed, error: %s", odf_file, strerror(errno));
        return NULL;
    }
    fseek(fp, 0, SEEK_END);
    long int len = ftell(fp);
    if (len <= 0 || len > 0x100000) {
        log_notice("File %s too big", odf_file);
        return NULL;
    }
    fseek(fp, 0, SEEK_SET);
    gchar *buf = g_new0(gchar, len + 1);
    size_t ret = fread(buf, 1, len, fp);
    if (ret != len) {
        g_free(buf);
        log_notice("Read %s failed, error: %s", odf_file, strerror(errno));
        return NULL;
    }
    buf[len] = '\0';
    *content = buf;
    return g_compute_checksum_for_data(G_CHECKSUM_SHA256, (const guchar *)buf, len);
}

static int _odf_validate(Connector object, const Connector_OdfValidate_Req *req, gpointer user_data)
{
    g_assert(req && req->odf_file);
    if (!req || !req->odf_file) {
        log_error("Parameter error");
        return -1;
    }
    cleanup_gfree gchar *content = NULL;

    cleanup_gfree gchar *sha256 = _get_file_sha256(req->odf_file, &content);
    if (!sha256) {
        log_error("Get file sha256 failed.");
        return -1;
    }
    G_LOCK(lock);
    validate_result *result = (validate_result *)g_hash_table_lookup(result_hash, req->odf_file);
    if (!result) {
        result = g_new0(validate_result, 1);
        g_hash_table_insert(result_hash, g_strdup(req->odf_file), (gpointer)result);
    } else if (g_strcmp0(result->sha256, sha256) == 0) {
        G_UNLOCK(lock);
        return result->result;
    } else {
        g_free(result->sha256);
    }

    result->sha256 = sha256;
    sha256 = NULL;

    result->result = _validate_odf_file(req->odf_file, content);
    G_UNLOCK(lock);
    return result->result;
}

int plugin_start(void)
{
    log_info("register odf validate plugin");
    so_hash = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
    result_hash = g_hash_table_new_full(g_str_hash, g_str_equal, NULL, NULL);
    Connector_OdfValidate_register("(s)", "()", _odf_validate, NULL);
    return 0;
}
