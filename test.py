import os
import subprocess
import unittest
from lbkit.tools import Tools

tool = Tools("component_test")
log = tool.log


class TestLitebmcClass(unittest.TestCase):
    rootfs_dir = os.environ.get("ROOTFS_DIR")

    @classmethod
    def tearDownClass(cls):
        tool.exec(f"rm -rf {cls.rootfs_dir}/opt/data/persistence")

    @classmethod
    def setUpClass(cls):
        if not os.environ.get("DBUS_SESSION_BUS_ADDRESS"):
            if not os.path.isfile("/dev/shm/session-dbus"):
                tool.pipe(["dbus-launch --sh-syntax"], out_file="/dev/shm/session-dbus")
            tool.exec(f"mkdir {cls.rootfs_dir}/var/run/dbus -p")
            tool.exec(f"cp /dev/shm/session-dbus {cls.rootfs_dir}/var/run/dbus/session-dbus")

        cls.rootfs_dir = os.environ.get("ROOTFS_DIR")
        log.info("export LD_LIBRARY_PATH={}".format(os.environ.get("LD_LIBRARY_PATH", "")))
        log.info("export ROOTFS_DIR={}".format(cls.rootfs_dir))

    def test_gdbusplus_pm_test(self):
        result = subprocess.run(f"gtester --verbose -k {self.rootfs_dir}/opt/litebmc/apps/odf_validate_test/odf_validate_test", shell=True)
        self.assertEqual(result.returncode, 0)


class LiteBmcComponentTest(object):
    """
    开发者测试

    lbk test命令执行时会查找组件源码目录下的test.py脚本的LiteBmcComponentTest任务类
    随后创建任务对象并执行test方法
    """
    # 测试文件所在目录，覆盖率统计时会忽略该目录下的源码文件
    test_src_folder = ["test_package"]

    def __init__(self, rootfs_dir):
        self.rootfs_dir = rootfs_dir
        os.environ["ROOTFS_DIR"] = rootfs_dir
        log.info("export LD_LIBRARY_PATH={}".format(os.environ.get("LD_LIBRARY_PATH", "")))
        log.info("export ROOTFS_DIR={}".format(rootfs_dir))
        pass

    def test(self, **kwargs) -> unittest.TestResult:
        """
        开发者测试

        启动测试，为便于扩展，必要时会传入字典类型的配置，具体传入什么样的值请参考lbkit开发者测试说明
        """
        suite = unittest.TestSuite()
        loader = unittest.TestLoader()
        tests = loader.loadTestsFromTestCase(TestLitebmcClass)
        suite.addTests(tests)
        runner = unittest.TextTestRunner(verbosity=2)
        return runner.run(suite)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            description="Test component", formatter_class=RawTextHelpFormatter)
    parser.add_argument("--rootfs_dir", default=".temp/rootfs",
                        help="Directory path of rootfs\nDefault .temp/rootfs")
    args = parser.parse_args()
    test = LiteBmcComponentTest(args.rootfs_dir)
    test.test()
