# lb_odf_validate

odf(object description file)是litebmc的对象描述文件，该文件在组件构建打包时完成两次json schema校验：
1. 公共json schema校验：使用ODF基本语法schema文件(`/usr/share/litebmc/schema/odf.v1.json`)完成基本语法校验
2. 接口json schema校验：使用每个接口单独生成的，存储在rootfs的`usr/share/litebmc/schema`目录下，由lbkit基于idf(interface description file)接口描述文件生成的接口schema文件完成专用校验。

上述两种校验方式能够按litebmc最佳实践开发的组件正确打包odf文件，但无法保证手工编辑、接口版本间兼容等问题，为此开发lb_odf_validate插件用于运行时校验。

#### 介绍

odf(object description file)验证插件，以支持com.litebmc.Connector加载数据验证ODF数据完整性。

#### 软件架构

odf校验依赖lbkit基于基于idf(interface description file)接口描述文件在接口public库中生成的validate函数。

具体实现是读取odf文件后验证基本的service、interface等成员后读取每一个objects成员，读取成员描述符中的interface接口名，随后从`LD_LIBRARY_PATH`路径下查找符合接口public库，运行库提供的一个validate接口验证odf数据合法性。

例如odf有一个接口`com.litebmc.Expression`的对象，lb_odf_validate会首先查找名称以`libcom.litebmc.expression-public.so`开头的动态库，随后查找并调用动态库提供的`com_litebmc_Expression_validate_odf`方法完成数据校验。

#### 我是一个插件

lb_odf_validate是一个litebmc插件，实现com.litebmc.Connector接口定义的`OdfValidate`动作，构建得到一个`liblb_odf_validate.so`插件，该插件会在gdbusplus启动时加载，调用插件的`pluging_start`方法完成`OdfValidate`钓子注册。

[`OdfValidate`接口定义PR](https://gitee.com/litebmc/interfaces/pulls/6)

gdbusplus的connector服务会在必要时候调用钓子完成odf文件完整性验证，当验证通过时返回0，否则返回-1。


#### 安装教程

插件仅依赖`com.litebmc.Connector-public`接口（间接依赖lb_base），安装在公共插件目录`/opt/litebmc/plugins`，不是其它组件的编译依赖，请不要在组件构建时引入lb_odf_validate依赖，建议由产品按需引入。

#### 使用说明

gdbusplus启动时会自动遍历公共插件目录下的插件，故仅需引入插件即可。

ODF校验失败会导致odf无法加载，请按照日志定位和解决问题。
