#include <dlfcn.h>
#include <sys/time.h>
#include <sys/stat.h>
#include "public/com.litebmc.Connector.h"

typedef int (*plugin_start)(void);
#define ODF_FILE "/tmp/odf_validate_test"
#define TEST_LOOP_CNT   1000


static void property_changed(LBO *object, const LBProperty *pop)
{
}

static gboolean emit_signal(LBO *object, const gchar *destination, const LBSignal *signal,
                            const void *msg, GError **error)
{
    return TRUE;
}
static guint subscribe_signal(const LBInterface *intf, const gchar *bus_name, const LBSignal *signal,
                              const gchar *object_path, const gchar *arg0, lbo_signal_handler handle,
                              gpointer user_data)
{
    return 0;
}
static void unsubscribe_signal(guint *id)
{
    *id = 0;
}
static int call_method(LBO *object, const LBMethod *method, const void *req, void **rsp,
                       gint timeout_msec, GError **error)
{
    return -1;
}

static gint write_property(LBO *object, const LBProperty *prop, GVariant *value, GError **error)
{
    return -1;
}

static gint read_property(LBO *object, const LBProperty *prop, GVariant **value, GError **error)
{
    return -1;
}
static LBImpl impl = {
    .property_changed = property_changed,
    .emit_signal = emit_signal,
    .subscribe_signal = subscribe_signal,
    .unsubscribe_signal = unsubscribe_signal,
    .call_method = call_method,
    .read_property = read_property,
    .write_property = write_property,
};

static void load_plugin(void)
{
    const gchar *rootfs_dir = g_getenv("ROOTFS_DIR");
    cleanup_gfree gchar *plugin_name = lb_printf("%s%s", rootfs_dir, "/opt/litebmc/plugins/connector/liblb_odf_validate.so");
    void *dl = dlopen(plugin_name, RTLD_NOW);
    g_assert_nonnull(dl);
    plugin_start start = (plugin_start)dlsym(dl, "plugin_start");
    g_assert_nonnull(start);
    int ret = start();
    g_assert_cmpint(ret, ==, 0);
}

static gint _valid_odf_str(const gchar *odf)
{
    FILE *fp = fopen(ODF_FILE, "w+");
    fwrite(odf, strlen(odf), 1, fp);
    fclose(fp);
    Connector_OdfValidate_Req req = {
        .odf_file = ODF_FILE
    };
    return Connector_OdfValidate_run(NULL, &req);
}

static void test_invalid_object_type(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    - interface: com.litebmc.Test";
    gint ret = _valid_odf_str(odf);
    g_assert_cmpint(ret, ==, -1);
}

static void test_invalid_bus_type(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: unknown\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Test";
    gint ret = _valid_odf_str(odf);
    g_assert_cmpint(ret, ==, -1);
}

static void test_invalid_objects_type(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  - /com/litebmc/Test:\n"
                       "      interface: com.litebmc.Test";
    gint ret = _valid_odf_str(odf);
    g_assert_cmpint(ret, ==, -1);
}

static void test_invalid_bus_type_type(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: [session]\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Test";
    gint ret = _valid_odf_str(odf);
    g_assert_cmpint(ret, ==, -1);
}

static void test_invalid_service_type(void)
{
    const gchar *odf = "service: [com.litebmc.Test]\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Test";
    gint ret = _valid_odf_str(odf);
    g_assert_cmpint(ret, ==, -1);
}

static void test_missing_objects(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session";
    gint ret = _valid_odf_str(odf);
    // 必须有objects
    g_assert_cmpint(ret, ==, -1);
}

static void test_missing_bus_type(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "objects:\n"
                       "/com/litebmc/Test:\n"
                       "    interface: com.litebmc.Test";
    gint ret = _valid_odf_str(odf);
    // 必须有bus_type
    g_assert_cmpint(ret, ==, -1);
}

static void test_missing_service(void)
{
    const gchar *odf = "bus_type: session\n"
                       "objects:\n"
                       "/com/litebmc/Test:\n"
                       "    interface: com.litebmc.Test";
    gint ret = _valid_odf_str(odf);
    // 必须有service
    g_assert_cmpint(ret, ==, -1);
}

static void test_valid_empty_content(void)
{
    const gchar *odf = "";
    gint ret = _valid_odf_str(odf);
    // 缺少必要成员
    g_assert_cmpint(ret, ==, -1);
}

static void test_file_permission_error(void)
{

    uid_t uid = getuid();
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Test";
    FILE *fp = fopen(ODF_FILE, "w+");
    fwrite(odf, strlen(odf), 1, fp);
    fclose(fp);

    // unlink file
    chmod(ODF_FILE, 0200);
    Connector_OdfValidate_Req req = {
        .odf_file = ODF_FILE
    };
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        gint ret = Connector_OdfValidate_run(NULL, &req);
        // 文件权限不正确
        if (uid == 0)
            g_assert_cmpint(ret, ==, 0);
        else
            g_assert_cmpint(ret, ==, -1);
    }
    chmod(ODF_FILE, 0600);
}

static void test_file_not_exist(void)
{
    // unlink file
    unlink(ODF_FILE);
    Connector_OdfValidate_Req req = {
        .odf_file = ODF_FILE
    };
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        gint ret = Connector_OdfValidate_run(NULL, &req);
        // 文件不存在
        g_assert_cmpint(ret, ==, -1);
    }
}

static void test_invlid_odf_file(void)
{
    const gchar *odf = "service: com.litebmc.Test"
                       "bus_type: session"
                       "objects:"
                       "  /com/litebmc/Test:"
                       "    interface: com.litebmc.Test";
    gint ret = _valid_odf_str(odf);
    // 不是有效的odf(yaml)文件格式
    g_assert_cmpint(ret, ==, -1);
}

static void test_invalid_object_name(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /:\n"
                       "    interface: com.litebmc.Test";
    // 非法的对象名
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        gint ret = _valid_odf_str(odf);
        g_assert_cmpint(ret, ==, -1);
    }
}

static void test_invalid_interface(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: /com/litebmc/Test";
    // interface格式不正确
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        gint ret = _valid_odf_str(odf);
        g_assert_cmpint(ret, ==, -1);
    }
    odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: [com.litebmc.Test]";
    // interface类型不正确
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        gint ret = _valid_odf_str(odf);
        g_assert_cmpint(ret, ==, -1);
    }
    odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    properties:\n"
                       "        BoolVal: false";
    // interface成员不存在
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        gint ret = _valid_odf_str(odf);
        g_assert_cmpint(ret, ==, -1);
    }
}

static void test_invalid_interface_type(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: [com.litebmc.Test]";
    // interface是数组，不符合要求
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        gint ret = _valid_odf_str(odf);
        g_assert_cmpint(ret, ==, -1);
    }
}

static void test_invalid_object_properties_is_string(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Test\n"
                       "    properties: com.litebmc.Test";
    // 对象类型为字符串
    gint ret = _valid_odf_str(odf);
    g_assert_cmpint(ret, ==, -1);
}

static void test_invalid_object_properties_is_array(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Test\n"
                       "    properties: [com.litebmc.Test]";
    // 对象类型为数组
    gint ret = _valid_odf_str(odf);
    g_assert_cmpint(ret, ==, -1);
}

static void test_valid_without_properties(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Test";
    // 非法的接口名
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        gint ret = _valid_odf_str(odf);
        g_assert_cmpint(ret, ==, 0);
    }
}

static void test_valid_object_of_interface_test(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Test\n"
                       "    properties:\n"
                       "      BoolVal: true\n"
                       "      ByteVal: 0xff\n"
                       "      I16Val: 0x7fff\n";
    // 合法的com.litebmc.Test对象
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        gint ret = _valid_odf_str(odf);
        g_assert_cmpint(ret, ==, 0);
    }
}

static void test_valid_object_of_interface_test_missing_required_prop(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Test\n"
                       "    properties:\n"
                       "      ByteVal: 0xff\n"
                       "      I16Val: 0x7fff\n";
    // com.litebmc.Test对象缺少必选(required)的BoolValue
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        gint ret = _valid_odf_str(odf);
        g_assert_cmpint(ret, ==, -1);
    }
}

static void test_valid_object_of_interface_test_with_error(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Test\n"
                       "    properties:\n"
                       "      BoolVal: unknown\n"
                       "      ByteVal: 0xffff\n"
                       "      I16Val: 65536\n";
    // 含有无法通过验证成员的com.litebmc.Test对象
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        gint ret = _valid_odf_str(odf);
        g_assert_cmpint(ret, ==, -1);
    }
}

static void test_valid_object_of_unknown_interface(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Unknown\n"
                       "    properties:\n"
                       "      BoolVal: true\n"
                       "      ByteVal: 0xff\n"
                       "      I16Val: 0x7fff\n";
    // 未知接口com.litebmc.Unknown
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        gint ret = _valid_odf_str(odf);
        g_assert_cmpint(ret, ==, -1);
    }
}

static void test_valid_object_of_interface_connector(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Connector\n"
                       "    properties:\n"
                       "      filename: base.yml\n"
                       "      position: 0x00\n";
    // 合法的com.litebmc.Connector对象
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        gint ret = _valid_odf_str(odf);
        g_assert_cmpint(ret, ==, 0);
    }
}

static void test_valid_object_of_interface_connector_with_error(void)
{
    const gchar *odf = "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Connector\n"
                       "    properties:\n"
                       "      filename: \"\"\n"
                       "      position: 0x100\n";
    // 含有无法通过验证成员的com.litebmc.Connector对象
    for (int i = 0; i < TEST_LOOP_CNT; i++) {
        gint ret = _valid_odf_str(odf);
        g_assert_cmpint(ret, ==, -1);
    }
}

static void test_xxx_odf_validator(const gchar *key, const gchar *val, gint assert_val)
{
    cleanup_gstring GString *str = g_string_sized_new(128);
    g_string_printf(str, "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Test\n"
                       "    properties:\n"
                       "      BoolVal: true\n"
                       "      %s: %s\n", key, val);
    gint ret = _valid_odf_str(str->str);
    g_assert_cmpint(ret, ==, assert_val);
}


static void test_arr_xxx_odf_validator(const gchar *key, const gchar *val, gint assert_val)
{
    cleanup_gstring GString *str = g_string_sized_new(128);
    g_string_printf(str, "service: com.litebmc.Test\n"
                       "bus_type: session\n"
                       "objects:\n"
                       "  /com/litebmc/Test:\n"
                       "    interface: com.litebmc.Test\n"
                       "    properties:\n"
                       "      BoolVal: true\n"
                       "      %s: [%s, %s]\n", key, val, val);
    gint ret = _valid_odf_str(str->str);
    g_assert_cmpint(ret, ==, assert_val);
}

static void test_byte_validator(void)
{
    const gchar *key = "U8Val";
    const gchar *arr_key = "ArrU8Val";
    test_arr_xxx_odf_validator(arr_key, "0", -1);
    test_xxx_odf_validator(key, "0", -1);
    test_arr_xxx_odf_validator(arr_key, "1", 0);
    test_xxx_odf_validator(key, "1", 0);
    test_arr_xxx_odf_validator(arr_key, "0xff", -1);
    test_xxx_odf_validator(key, "0xff", -1);
    test_arr_xxx_odf_validator(arr_key, "0xfe", 0);
    test_xxx_odf_validator(key, "0xfe", 0);
}
static void test_uint16_validator(void)
{
    const gchar *key = "Uint16Val";
    const gchar *arr_key = "ArrUint16Val";
    test_arr_xxx_odf_validator(arr_key, "0", -1);
    test_xxx_odf_validator(key, "0", -1);
    test_arr_xxx_odf_validator(arr_key, "1", 0);
    test_xxx_odf_validator(key, "1", 0);
    test_arr_xxx_odf_validator(arr_key, "0xffff", -1);
    test_xxx_odf_validator(key, "0xffff", -1);
    test_arr_xxx_odf_validator(arr_key, "0xfffe", 0);
    test_xxx_odf_validator(key, "0xfffe", 0);
}
static void test_int16_validator(void)
{
    const gchar *key = "Int16Val";
    const gchar *arr_key = "ArrInt16Val";
    test_arr_xxx_odf_validator(arr_key, "-32768", -1);
    test_xxx_odf_validator(key, "-32768", -1);
    test_arr_xxx_odf_validator(arr_key, "-32767", 0);
    test_xxx_odf_validator(key, "-32767", 0);
    test_arr_xxx_odf_validator(arr_key, "32767", -1);
    test_xxx_odf_validator(key, "32767", -1);
    test_arr_xxx_odf_validator(arr_key, "32766", 0);
    test_xxx_odf_validator(key, "32766", 0);
}
static void test_uint32_validator(void)
{
    const gchar *key = "Uint32Val";
    const gchar *arr_key = "ArrUint32Val";
    test_arr_xxx_odf_validator(arr_key, "0", -1);
    test_xxx_odf_validator(key, "0", -1);
    test_arr_xxx_odf_validator(arr_key, "1", 0);
    test_xxx_odf_validator(key, "1", 0);
    test_arr_xxx_odf_validator(arr_key, "0xffffffff", -1);
    test_xxx_odf_validator(key, "0xffffffff", -1);
    test_arr_xxx_odf_validator(arr_key, "0xfffffffe", 0);
    test_xxx_odf_validator(key, "0xfffffffe", 0);
}
static void test_int32_validator(void)
{
    const gchar *key = "Int32Val";
    const gchar *arr_key = "ArrInt32Val";
    test_arr_xxx_odf_validator(arr_key, "-2147483648", -1);
    test_xxx_odf_validator(key, "-2147483648", -1);
    test_arr_xxx_odf_validator(arr_key, "-2147483647", 0);
    test_xxx_odf_validator(key, "-2147483647", 0);
    test_arr_xxx_odf_validator(arr_key, "2147483647", -1);
    test_xxx_odf_validator(key, "2147483647", -1);
    test_arr_xxx_odf_validator(arr_key, "2147483646", 0);
    test_xxx_odf_validator(key, "2147483646", 0);
}
static void test_uint64_validator(void)
{
    const gchar *key = "Uint64Val";
    const gchar *arr_key = "ArrUint64Val";
    test_arr_xxx_odf_validator(arr_key, "0", -1);
    test_xxx_odf_validator(key, "0", -1);
    test_arr_xxx_odf_validator(arr_key, "1", 0);
    test_xxx_odf_validator(key, "1", 0);
    test_arr_xxx_odf_validator(arr_key, "0xffffffffffffffff", -1);
    test_xxx_odf_validator(key, "0xffffffffffffffff", -1);
    test_arr_xxx_odf_validator(arr_key, "0xfffffffffffffffe", 0);
    test_xxx_odf_validator(key, "0xfffffffffffffffe", 0);
}
static void test_int64_validator(void)
{
    const gchar *key = "Int64Val";
    const gchar *arr_key = "ArrInt64Val";
    test_arr_xxx_odf_validator(arr_key, "-9223372036854775808", -1);
    test_xxx_odf_validator(key, "-9223372036854775808", -1);
    test_arr_xxx_odf_validator(arr_key, "-9223372036854775807", 0);
    test_xxx_odf_validator(key, "-9223372036854775807", 0);
    test_arr_xxx_odf_validator(arr_key, "9223372036854775807", -1);
    test_xxx_odf_validator(key, "9223372036854775807", -1);
    test_arr_xxx_odf_validator(arr_key, "9223372036854775806", 0);
    test_xxx_odf_validator(key, "9223372036854775806", 0);
}
static void test_double_validator(void)
{
    const gchar *key = "DoubleVal";
    const gchar *arr_key = "ArrDoubleVal";
    test_arr_xxx_odf_validator(arr_key, "-10000.1", -1);
    test_xxx_odf_validator(key, "-10000.1", -1);
    test_arr_xxx_odf_validator(arr_key, "-10000", 0);
    test_xxx_odf_validator(key, "-10000", 0);
    test_arr_xxx_odf_validator(arr_key, "10000.1", -1);
    test_xxx_odf_validator(key, "10000.1", -1);
    test_arr_xxx_odf_validator(arr_key, "10000", 0);
    test_xxx_odf_validator(key, "10000", 0);
}
static void test_double_exclusive_validator(void)
{
    const gchar *key = "DoubleValExclusive";
    const gchar *arr_key = "ArrDoubleValExclusive";
    test_arr_xxx_odf_validator(arr_key, "-10000", -1);
    test_xxx_odf_validator(key, "-10000", -1);
    test_arr_xxx_odf_validator(arr_key, "-9999.1", 0);
    test_xxx_odf_validator(key, "-9999.1", 0);
    test_arr_xxx_odf_validator(arr_key, "10000", -1);
    test_xxx_odf_validator(key, "10000", -1);
    test_arr_xxx_odf_validator(arr_key, "9999.9", 0);
    test_xxx_odf_validator(key, "9999.9", 0);
}
static void test_string_validator(void)
{
    const gchar *key = "StringVal";
    const gchar *arr_key = "ArrStringVal";
    // 正则表达式为：^[a-z]{1,2}$
    test_arr_xxx_odf_validator(arr_key, "aaa", -1);
    test_xxx_odf_validator(key, "aaa", -1);
    test_arr_xxx_odf_validator(arr_key, "aa", 0);
    test_xxx_odf_validator(key, "aa", 0);
    test_arr_xxx_odf_validator(arr_key, "a", 0);
    test_xxx_odf_validator(key, "a", 0);
    test_arr_xxx_odf_validator(arr_key, "", -1);
    test_xxx_odf_validator(key, "", -1);
}
static void test_object_path_validator(void)
{
    const gchar *key = "ObjectPathVal";
    const gchar *arr_key = "ArrObjectPathVal";
    // 正则表达式："^/com/litebmc/[a-z]{1,2}$"
    test_arr_xxx_odf_validator(arr_key, "/com/litebmc/aaa", -1);
    test_xxx_odf_validator(key, "/com/litebmc/aaa", -1);
    test_arr_xxx_odf_validator(arr_key, "/com/litebmc/aa", 0);
    test_xxx_odf_validator(key, "/com/litebmc/aa", 0);
    test_arr_xxx_odf_validator(arr_key, "/com/litebmc/a", 0);
    test_xxx_odf_validator(key, "/com/litebmc/a", 0);
    test_arr_xxx_odf_validator(arr_key, "/com/litebmc/", -1);
    test_xxx_odf_validator(key, "/com/litebmc/", -1);
    test_arr_xxx_odf_validator(arr_key, "com/litebmc/aa", -1);
    test_xxx_odf_validator(key, "com/litebmc/aa", -1);
}

static void test_signature_validator(void)
{
    const gchar *key = "SignatureVal";
    const gchar *arr_key = "ArrSignatureVal";
    // 正则表达式: "^as[i]{1,2}$"
    test_arr_xxx_odf_validator(arr_key, "asiii", -1);
    test_xxx_odf_validator(key, "asiii", -1);
    test_arr_xxx_odf_validator(arr_key, "asii", 0);
    test_xxx_odf_validator(key, "asii", 0);
    test_arr_xxx_odf_validator(arr_key, "asi", 0);
    test_xxx_odf_validator(key, "asi", 0);
    test_arr_xxx_odf_validator(arr_key, "as", -1);
    test_xxx_odf_validator(key, "as", -1);
    test_arr_xxx_odf_validator(arr_key, "aasi", -1);
    test_xxx_odf_validator(key, "aasi", -1);
}

#ifdef LB_CODEGEN_BE_5_2
#define test_validate_match_type(key, val, expect)                                                                     \
    static void test_validate_match_##key(void)                                                                        \
    {                                                                                                                  \
        const gchar *odf = "service: com.litebmc.Test\n"                                                               \
                           "bus_type: session\n"                                                                       \
                           "objects:\n"                                                                                \
                           "  /com/litebmc/Test:\n"                                                                    \
                           "    interface: com.litebmc.Test\n"                                                         \
                           "    properties:\n"                                                                         \
                           "        BoolVal: false\n        " val "\n";                                                \
        /* match_byte符合要求 */                                                                                   \
        gint ret = _valid_odf_str(odf);                                                                                \
        g_assert_cmpint(ret, ==, expect);                                                                              \
    }

// 存在的成员
test_validate_match_type(double, "match_double: 121", 0)
test_validate_match_type(int16, "match_int16: 121", 0)
test_validate_match_type(int32, "match_int32: 121", 0)
test_validate_match_type(int64, "match_int64: 121", 0)
test_validate_match_type(uint8, "match_byte: 121", 0)
test_validate_match_type(uint16, "match_uint16: 121", 0)
test_validate_match_type(uint32, "match_uint32: 121", 0)
test_validate_match_type(uint64, "match_uint64: 121", 0)
test_validate_match_type(size, "match_size: 121", 0)
test_validate_match_type(ssize, "match_ssize: 121", 0)
test_validate_match_type(string, "match_string: abc", 0)
test_validate_match_type(object_path, "match_object_path: /abc/121", 0)
test_validate_match_type(signature, "match_signature: a(ss)", 0)

test_validate_match_type(double_array, "match_array_double: [121]", 0)
test_validate_match_type(int16_array, "match_array_int16: [121]", 0)
test_validate_match_type(int32_array, "match_array_int32: [121]", 0)
test_validate_match_type(int64_array, "match_array_int64: [121]", 0)
test_validate_match_type(uint8_array, "match_array_byte: [121]", 0)
test_validate_match_type(uint16_array, "match_array_uint16: [121]", 0)
test_validate_match_type(uint32_array, "match_array_uint32: [121]", 0)
test_validate_match_type(uint64_array, "match_array_uint64: [121]", 0)
test_validate_match_type(size_array, "match_array_size: [121]", 0)
test_validate_match_type(ssize_array, "match_array_ssize: [121]", 0)
test_validate_match_type(string_array, "match_array_string: [abc]", 0)
test_validate_match_type(object_path_array, "match_array_object_path: [/abc/121]", 0)
test_validate_match_type(signature_array, "match_array_signature: [a(ss)]", 0)

// 不存在的成员
test_validate_match_type(double_false, "match_double: 122", -1)
test_validate_match_type(int16_false, "match_int16: 122", -1)
test_validate_match_type(int32_false, "match_int32: 122", -1)
test_validate_match_type(int64_false, "match_int64: 122", -1)
test_validate_match_type(uint8_false, "match_byte: 122", -1)
test_validate_match_type(uint16_false, "match_uint16: 122", -1)
test_validate_match_type(uint32_false, "match_uint32: 122", -1)
test_validate_match_type(uint64_false, "match_uint64: 122", -1)
test_validate_match_type(size_false, "match_size: 122", -1)
test_validate_match_type(ssize_false, "match_ssize: 122", -1)
test_validate_match_type(string_false, "match_string: abe", -1)
test_validate_match_type(object_path_false, "match_object_path: /abc/12a", -1)
test_validate_match_type(signature_false, "match_signature: a(ii)", -1)

test_validate_match_type(double_array_false, "match_array_double: [122]", -1)
test_validate_match_type(int16_array_false, "match_array_int16: [122]", -1)
test_validate_match_type(int32_array_false, "match_array_int32: [122]", -1)
test_validate_match_type(int64_array_false, "match_array_int64: [122]", -1)
test_validate_match_type(uint8_array_false, "match_array_byte: [122]", -1)
test_validate_match_type(uint16_array_false, "match_array_uint16: [122]", -1)
test_validate_match_type(uint32_array_false, "match_array_uint32: [122]", -1)
test_validate_match_type(uint64_array_false, "match_array_uint64: [122]", -1)
test_validate_match_type(size_array_false, "match_array_size: [122]", -1)
test_validate_match_type(ssize_array_false, "match_array_ssize: [122]", -1)
test_validate_match_type(string_array_false, "match_array_string: [abe]", -1)
test_validate_match_type(object_path_array_false, "match_array_object_path: [/abc/12a]", -1)
test_validate_match_type(signature_array_false, "match_array_signature: [a(ii)]", -1);

/* 定义成员数量校验函数 */
#define test_validate_items_count(key, prop, val1, val2, val3)                                                         \
    static void test_validate_items_count_##key(void)                                                                  \
    {                                                                                                                  \
        const gchar *odf = NULL;                                                                                       \
        odf = "service: com.litebmc.Test\n"                                                                            \
              "bus_type: session\n"                                                                                    \
              "objects:\n"                                                                                             \
              "  /com/litebmc/Test:\n"                                                                                 \
              "    interface: com.litebmc.Test\n"                                                                      \
              "    properties:\n"                                                                                      \
              "        BoolVal: false\n        " prop ": []\n";                                                        \
        /* 0个成员，不符合要求 */                                                                             \
        gint ret = _valid_odf_str(odf);                                                                                \
        g_assert_cmpint(ret, ==, -1);                                                                                  \
        odf = "service: com.litebmc.Test\n"                                                                            \
              "bus_type: session\n"                                                                                    \
              "objects:\n"                                                                                             \
              "  /com/litebmc/Test:\n"                                                                                 \
              "    interface: com.litebmc.Test\n"                                                                      \
              "    properties:\n"                                                                                      \
              "        BoolVal: false\n        " prop ": [" val1 "]\n";                                                \
        /* 1个成员，符合要求 */                                                                                \
        ret = _valid_odf_str(odf);                                                                                     \
        g_assert_cmpint(ret, ==, 0);                                                                                   \
        odf = "service: com.litebmc.Test\n"                                                                            \
              "bus_type: session\n"                                                                                    \
              "objects:\n"                                                                                             \
              "  /com/litebmc/Test:\n"                                                                                 \
              "    interface: com.litebmc.Test\n"                                                                      \
              "    properties:\n"                                                                                      \
              "        BoolVal: false\n        " prop ": [" val1 ", " val2 "]\n";                                      \
        /* 2个成员，符合要求 */                                                                                \
        ret = _valid_odf_str(odf);                                                                                     \
        g_assert_cmpint(ret, ==, 0);                                                                                   \
        odf = "service: com.litebmc.Test\n"                                                                            \
              "bus_type: session\n"                                                                                    \
              "objects:\n"                                                                                             \
              "  /com/litebmc/Test:\n"                                                                                 \
              "    interface: com.litebmc.Test\n"                                                                      \
              "    properties:\n"                                                                                      \
              "        BoolVal: false\n        " prop ": [" val1 ", " val2 ", " val3 "]\n";                            \
        /* 3个成员，不符合要求 */                                                                             \
        ret = _valid_odf_str(odf);                                                                                     \
        g_assert_cmpint(ret, ==, -1);                                                                                  \
    }

test_validate_items_count(boolean, "match_array_boolean", "true", "true", "true");
test_validate_items_count(double, "match_array_double", "121", "234", "250");
test_validate_items_count(int16, "match_array_int16", "121", "234", "250");
test_validate_items_count(int32, "match_array_int32", "121", "234", "250");
test_validate_items_count(int64, "match_array_int64", "121", "234", "250");
test_validate_items_count(uint8, "match_array_byte", "121", "234", "250");
test_validate_items_count(uint16, "match_array_uint16", "121", "234", "250");
test_validate_items_count(uint32, "match_array_uint32", "121", "234", "250");
test_validate_items_count(uint64, "match_array_uint64", "121", "234", "250");
test_validate_items_count(size, "match_array_size", "121", "234", "250");
test_validate_items_count(ssize, "match_array_ssize", "121", "234", "250");
test_validate_items_count(string, "match_array_string", "abc", "efg", "hij");
test_validate_items_count(object_path, "match_array_object_path", "/abc/121", "/efg/234", "/hij/250");
test_validate_items_count(signature, "match_array_signature", "\"a(ss)\"", "\"a{ss}\"", "\"a{st}\"");

/* 定义成员数量校验函数 */
#define test_validate_unique_items(key, prop, val1)                                                                    \
    static void test_validate_unique_items_##key(void)                                                                 \
    {                                                                                                                  \
        const gchar *odf = NULL;                                                                                       \
        gint ret = 0;                                                                                                  \
        odf = "service: com.litebmc.Test\n"                                                                            \
              "bus_type: session\n"                                                                                    \
              "objects:\n"                                                                                             \
              "  /com/litebmc/Test:\n"                                                                                 \
              "    interface: com.litebmc.Test\n"                                                                      \
              "    properties:\n"                                                                                      \
              "        BoolVal: false\n        " prop ": [" val1 "]\n";                                                \
        /* 1个成员，符合要求 */                                                                                \
        ret = _valid_odf_str(odf);                                                                                     \
        g_assert_cmpint(ret, ==, 0);                                                                                   \
        odf = "service: com.litebmc.Test\n"                                                                            \
              "bus_type: session\n"                                                                                    \
              "objects:\n"                                                                                             \
              "  /com/litebmc/Test:\n"                                                                                 \
              "    interface: com.litebmc.Test\n"                                                                      \
              "    properties:\n"                                                                                      \
              "        BoolVal: false\n        " prop ": [" val1 ", " val1 "]\n";                                      \
        /* 2个相同成员，不符合要求 */                                                                       \
        ret = _valid_odf_str(odf);                                                                                     \
        g_assert_cmpint(ret, ==, -1);                                                                                  \
    }

test_validate_unique_items(double, "match_array_double", "121");
test_validate_unique_items(int16, "match_array_int16", "121");
test_validate_unique_items(int32, "match_array_int32", "121");
test_validate_unique_items(int64, "match_array_int64", "121");
test_validate_unique_items(uint8, "match_array_byte", "121");
test_validate_unique_items(uint16, "match_array_uint16", "121");
test_validate_unique_items(uint32, "match_array_uint32", "121");
test_validate_unique_items(uint64, "match_array_uint64", "121");
test_validate_unique_items(size, "match_array_size", "121");
test_validate_unique_items(ssize, "match_array_ssize", "121");
test_validate_unique_items(string, "match_array_string", "abc");
test_validate_unique_items(object_path, "match_array_object_path", "/abc/121");
test_validate_unique_items(signature, "match_array_signature", "\"a(ss)\"");

#endif
void test_add_test_case(void)
{
    if (lb_log_get_type() == LOG_TO_FILE) {
        lb_log_set_level(LOG_INFO);
    }
    struct timeval tv;
    gettimeofday(&tv, NULL);
    srand(tv.tv_usec);
    g_test_add_func("/test/valid_without_properties", test_valid_without_properties);
    g_test_add_func("/test/valid_empty_content", test_valid_empty_content);
    g_test_add_func("/test/missing_service", test_missing_service);
    g_test_add_func("/test/missing_bus_type", test_missing_bus_type);
    g_test_add_func("/test/missing_object", test_missing_objects);
    g_test_add_func("/test/invalid_service_type", test_invalid_service_type);
    g_test_add_func("/test/invalid_objects_type", test_invalid_objects_type);
    g_test_add_func("/test/invalid_bus_type_type", test_invalid_bus_type_type);
    g_test_add_func("/test/invalid_object_type", test_invalid_object_type);
    g_test_add_func("/test/invalid_bus_type", test_invalid_bus_type);
    g_test_add_func("/test/file_permission_error", test_file_permission_error);
    g_test_add_func("/test/file_not_exist", test_file_not_exist);
    g_test_add_func("/test/invlid_odf_file", test_invlid_odf_file);
    g_test_add_func("/test/invalid_object_name", test_invalid_object_name);
    g_test_add_func("/test/invalid_interface", test_invalid_interface);
    g_test_add_func("/test/invalid_interface_type", test_invalid_interface_type);
    g_test_add_func("/test/invalid_objects_properties_is_string", test_invalid_object_properties_is_string);
    g_test_add_func("/test/invalid_objects_properties_is_array", test_invalid_object_properties_is_array);
    g_test_add_func("/test/valid_object_of_interface_test", test_valid_object_of_interface_test);
    g_test_add_func("/test/valid_object_of_interface_test_missing_required_prop", test_valid_object_of_interface_test_missing_required_prop);
    g_test_add_func("/test/valid_object_of_interface_test_with_error", test_valid_object_of_interface_test_with_error);
    g_test_add_func("/test/valid_object_of_interface_connector", test_valid_object_of_interface_connector);
    g_test_add_func("/test/valid_object_of_interface_connector_with_error", test_valid_object_of_interface_connector_with_error);
    g_test_add_func("/test/valid_object_of_unknown_interface", test_valid_object_of_unknown_interface);
    g_test_add_func("/test/byte_validator", test_byte_validator);
    g_test_add_func("/test/uint16_validator", test_uint16_validator);
    g_test_add_func("/test/int16_validator", test_int16_validator);
    g_test_add_func("/test/uint32_validator", test_uint32_validator);
    g_test_add_func("/test/int32_validator", test_int32_validator);
    g_test_add_func("/test/uint64_validator", test_uint64_validator);
    g_test_add_func("/test/int64_validator", test_int64_validator);
    g_test_add_func("/test/double_validator", test_double_validator);
    g_test_add_func("/test/double_exclusive_validator", test_double_exclusive_validator);
    g_test_add_func("/test/string_validator", test_string_validator);
    g_test_add_func("/test/object_path_validator", test_object_path_validator);
    g_test_add_func("/test/signature_validator", test_signature_validator);
#ifdef LB_CODEGEN_BE_5_2
    // 标量匹配场景
    g_test_add_func("/test/validate_match_double", test_validate_match_double);
    g_test_add_func("/test/validate_match_int16", test_validate_match_int16);
    g_test_add_func("/test/validate_match_int32", test_validate_match_int32);
    g_test_add_func("/test/validate_match_int64", test_validate_match_int64);
    g_test_add_func("/test/validate_match_uint8", test_validate_match_uint8);
    g_test_add_func("/test/validate_match_uint16", test_validate_match_uint16);
    g_test_add_func("/test/validate_match_uint32", test_validate_match_uint32);
    g_test_add_func("/test/validate_match_uint64", test_validate_match_uint64);
    g_test_add_func("/test/validate_match_size", test_validate_match_size);
    g_test_add_func("/test/validate_match_ssize", test_validate_match_ssize);
    g_test_add_func("/test/validate_match_string", test_validate_match_string);
    g_test_add_func("/test/validate_match_object_path", test_validate_match_object_path);
    g_test_add_func("/test/validate_match_signature", test_validate_match_signature);
    // 标量不匹配场景
    g_test_add_func("/test/validate_match_double_false", test_validate_match_double_false);
    g_test_add_func("/test/validate_match_int16_false", test_validate_match_int16_false);
    g_test_add_func("/test/validate_match_int32_false", test_validate_match_int32_false);
    g_test_add_func("/test/validate_match_int64_false", test_validate_match_int64_false);
    g_test_add_func("/test/validate_match_uint8_false", test_validate_match_uint8_false);
    g_test_add_func("/test/validate_match_uint16_false", test_validate_match_uint16_false);
    g_test_add_func("/test/validate_match_uint32_false", test_validate_match_uint32_false);
    g_test_add_func("/test/validate_match_uint64_false", test_validate_match_uint64_false);
    g_test_add_func("/test/validate_match_size_false", test_validate_match_size_false);
    g_test_add_func("/test/validate_match_ssize_false", test_validate_match_ssize_false);
    g_test_add_func("/test/validate_match_string_false", test_validate_match_string_false);
    g_test_add_func("/test/validate_match_object_path_false", test_validate_match_object_path_false);
    g_test_add_func("/test/validate_match_signature_false", test_validate_match_signature_false);
    // 数据匹配场景
    g_test_add_func("/test/validate_match_double_array", test_validate_match_double_array);
    g_test_add_func("/test/validate_match_int16_array", test_validate_match_int16_array);
    g_test_add_func("/test/validate_match_int32_array", test_validate_match_int32_array);
    g_test_add_func("/test/validate_match_int64_array", test_validate_match_int64_array);
    g_test_add_func("/test/validate_match_uint8_array", test_validate_match_uint8_array);
    g_test_add_func("/test/validate_match_uint16_array", test_validate_match_uint16_array);
    g_test_add_func("/test/validate_match_uint32_array", test_validate_match_uint32_array);
    g_test_add_func("/test/validate_match_uint64_array", test_validate_match_uint64_array);
    g_test_add_func("/test/validate_match_size_array", test_validate_match_size_array);
    g_test_add_func("/test/validate_match_ssize_array", test_validate_match_ssize_array);
    g_test_add_func("/test/validate_match_string_array", test_validate_match_string_array);
    g_test_add_func("/test/validate_match_object_path_array", test_validate_match_object_path_array);
    g_test_add_func("/test/validate_match_signature_array", test_validate_match_signature_array);
    // 数组不匹配
    g_test_add_func("/test/validate_match_double_array_false", test_validate_match_double_array_false);
    g_test_add_func("/test/validate_match_int16_array_false", test_validate_match_int16_array_false);
    g_test_add_func("/test/validate_match_int32_array_false", test_validate_match_int32_array_false);
    g_test_add_func("/test/validate_match_int64_array_false", test_validate_match_int64_array_false);
    g_test_add_func("/test/validate_match_uint8_array_false", test_validate_match_uint8_array_false);
    g_test_add_func("/test/validate_match_uint16_array_false", test_validate_match_uint16_array_false);
    g_test_add_func("/test/validate_match_uint32_array_false", test_validate_match_uint32_array_false);
    g_test_add_func("/test/validate_match_uint64_array_false", test_validate_match_uint64_array_false);
    g_test_add_func("/test/validate_match_size_array_false", test_validate_match_size_array_false);
    g_test_add_func("/test/validate_match_ssize_array_false", test_validate_match_ssize_array_false);
    g_test_add_func("/test/validate_match_string_array_false", test_validate_match_string_array_false);
    g_test_add_func("/test/validate_match_object_path_array_false", test_validate_match_object_path_array_false);
    g_test_add_func("/test/validate_match_signature_array_false", test_validate_match_signature_array_false);
    // 数量校验
    g_test_add_func("/test/validate_items_count_boolean", test_validate_items_count_boolean);
    g_test_add_func("/test/validate_items_count_double", test_validate_items_count_double);
    g_test_add_func("/test/validate_items_count_int16", test_validate_items_count_int16);
    g_test_add_func("/test/validate_items_count_int32", test_validate_items_count_int32);
    g_test_add_func("/test/validate_items_count_int64", test_validate_items_count_int64);
    g_test_add_func("/test/validate_items_count_uint8", test_validate_items_count_uint8);
    g_test_add_func("/test/validate_items_count_uint16", test_validate_items_count_uint16);
    g_test_add_func("/test/validate_items_count_uint32", test_validate_items_count_uint32);
    g_test_add_func("/test/validate_items_count_uint64", test_validate_items_count_uint64);
    g_test_add_func("/test/validate_items_count_size", test_validate_items_count_size);
    g_test_add_func("/test/validate_items_count_ssize", test_validate_items_count_ssize);
    g_test_add_func("/test/validate_items_count_string", test_validate_items_count_string);
    g_test_add_func("/test/validate_items_count_object_path", test_validate_items_count_object_path);
    g_test_add_func("/test/validate_items_count_signature", test_validate_items_count_signature);
    // 唯一性校验
    g_test_add_func("/test/validate_unique_items_double", test_validate_unique_items_double);
    g_test_add_func("/test/validate_unique_items_int16", test_validate_unique_items_int16);
    g_test_add_func("/test/validate_unique_items_int32", test_validate_unique_items_int32);
    g_test_add_func("/test/validate_unique_items_int64", test_validate_unique_items_int64);
    g_test_add_func("/test/validate_unique_items_uint8", test_validate_unique_items_uint8);
    g_test_add_func("/test/validate_unique_items_uint16", test_validate_unique_items_uint16);
    g_test_add_func("/test/validate_unique_items_uint32", test_validate_unique_items_uint32);
    g_test_add_func("/test/validate_unique_items_uint64", test_validate_unique_items_uint64);
    g_test_add_func("/test/validate_unique_items_size", test_validate_unique_items_size);
    g_test_add_func("/test/validate_unique_items_ssize", test_validate_unique_items_ssize);
    g_test_add_func("/test/validate_unique_items_string", test_validate_unique_items_string);
    g_test_add_func("/test/validate_unique_items_object_path", test_validate_unique_items_object_path);
    g_test_add_func("/test/validate_unique_items_signature", test_validate_unique_items_signature);
#endif
}

int main(int argc, char *argv[])
{
    unlink("/dev/shm/per.db");
    lb_init(&impl);
    load_plugin();
    g_test_init(&argc, &argv, NULL);
    test_add_test_case();
    return g_test_run();
}
